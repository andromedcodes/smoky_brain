package com.codes.andromed.mapplication.home.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.codes.andromed.mapplication.AnimatedExpandableListView;
import com.codes.andromed.mapplication.R;
import com.codes.andromed.mapplication.home.adapters.listviewadapters.AnimatedEXPLVAdapter;
import com.codes.andromed.mapplication.models.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Categories extends Fragment {

    private AnimatedExpandableListView expandableListView;


    public Categories() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_categories, container, false);

        List<String> categoriesList = new ArrayList<>();
        categoriesList.add("Category 1");
        categoriesList.add("Category 2");
        categoriesList.add("Category 3");

        List<Product> pl1 = new ArrayList<>();
        pl1.add(new Product());
        pl1.add(new Product());
        pl1.add(new Product());

        List<Product> pl2 = new ArrayList<>();
        pl2.add(new Product());
        pl2.add(new Product());
        pl2.add(new Product());

        List<Product> pl3 = new ArrayList<>();
        pl3.add(new Product());
        pl3.add(new Product());
        pl3.add(new Product());


        HashMap<String, List<Product>> childrenList = new HashMap<>();
        childrenList.put(categoriesList.get(0), pl1);
        childrenList.put(categoriesList.get(1), pl2);
        childrenList.put(categoriesList.get(2), pl3);


        expandableListView = (AnimatedExpandableListView) rootView.findViewById(R.id.exp_lv_Categories);
        /*expandableListView.setAdapter(new ExpandableCategoryAdapter(rootView.getContext(),
                categoriesList, childrenList));*/

        expandableListView.setAdapter(new AnimatedEXPLVAdapter(rootView.getContext(),
                categoriesList, childrenList));

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // We call collapseGroupWithAnimation(int) and
                // expandGroupWithAnimation(int) to animate group
                // expansion/collapse.
                if (expandableListView.isGroupExpanded(groupPosition)) {
                    expandableListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    expandableListView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }

        });

        return rootView;
    }

}

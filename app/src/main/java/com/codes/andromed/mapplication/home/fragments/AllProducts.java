package com.codes.andromed.mapplication.home.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.codes.andromed.mapplication.R;
import com.codes.andromed.mapplication.home.adapters.listviewadapters.ItemListAdapter;
import com.codes.andromed.mapplication.models.Product;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllProducts extends Fragment {


    public AllProducts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ListView lvProductList;

        // Inflate the layout for this fragment
        final View rootView =  inflater.inflate(R.layout.fragment_all_products, container, false);

        ArrayList<Product> productList = new ArrayList<>();
        productList.add(new Product());
        productList.add(new Product());
        productList.add(new Product());
        productList.add(new Product());
        productList.add(new Product());

        lvProductList = (ListView) rootView.findViewById(R.id.lv_AllProducts);
        lvProductList.setAdapter(new ItemListAdapter(rootView.getContext(), productList));

        return rootView;
    }
}

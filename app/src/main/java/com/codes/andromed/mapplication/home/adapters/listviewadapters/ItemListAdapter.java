package com.codes.andromed.mapplication.home.adapters.listviewadapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.codes.andromed.mapplication.R;
import com.codes.andromed.mapplication.models.Product;
import com.codes.andromed.mapplication.productdetail.ProductDetails;

import java.util.ArrayList;

/**
 * Created by Utilisateur on 07/12/2016.
 */
public class ItemListAdapter extends BaseAdapter {

    private ArrayList<Product> products;
    private Context mContext;
    private LayoutInflater inflater = null;
    private ViewHolder viewHolder;

    static class ViewHolder {
        public TextView tvCenterTopTitle;
        public TextView tvCenterBottomTitle;
        public TextView tvLeftTitle;
        public TextView tvRightTitle;
        public ImageView ivLeftImage;
        public ImageView ivRightImage;
    }

    public ItemListAdapter(Context context, ArrayList<Product> products) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        this.products = products;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.indexOf(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Product mProduct = products.get(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.lv_item_sfr, null);
            viewHolder = new ViewHolder();
            viewHolder.tvCenterTopTitle = (TextView) convertView.findViewById(R.id.tv_center_top_title);
            viewHolder.tvCenterBottomTitle = (TextView) convertView.findViewById(R.id.tv_center_bottom_title);
            viewHolder.tvLeftTitle = (TextView) convertView.findViewById(R.id.tv_left_title);
            viewHolder.tvRightTitle = (TextView) convertView.findViewById(R.id.tv_right_title);
            viewHolder.ivLeftImage = (ImageView) convertView.findViewById(R.id.iv_left_image);
            viewHolder.ivRightImage = (ImageView) convertView.findViewById(R.id.iv_right_image);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, ProductDetails.class),
                        ActivityOptions.makeSceneTransitionAnimation((Activity) mContext).toBundle());
            }
        });
        return convertView;
    }
}

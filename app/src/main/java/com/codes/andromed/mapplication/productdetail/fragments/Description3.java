package com.codes.andromed.mapplication.productdetail.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codes.andromed.mapplication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Description3 extends Fragment {


    public Description3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_description3, container, false);
    }

}

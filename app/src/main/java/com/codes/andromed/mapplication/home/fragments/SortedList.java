package com.codes.andromed.mapplication.home.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codes.andromed.mapplication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SortedList extends Fragment {


    public SortedList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sorted_list, container, false);
    }

}

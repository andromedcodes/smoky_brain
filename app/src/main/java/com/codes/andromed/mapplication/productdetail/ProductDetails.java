package com.codes.andromed.mapplication.productdetail;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.view.View;
import android.view.Window;

import com.codes.andromed.mapplication.R;
import com.codes.andromed.mapplication.productdetail.adapters.PDViewPagerAdapter;
import com.codes.andromed.mapplication.productdetail.fragments.Description1;
import com.codes.andromed.mapplication.productdetail.fragments.Description2;
import com.codes.andromed.mapplication.productdetail.fragments.Description3;

public class ProductDetails extends AppCompatActivity {

    private Toolbar mToolBar;
    private ViewPager mViewPager;
    private PDViewPagerAdapter mPDViewPagerAdapter;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setEnterTransition(new Explode());
        getWindow().setExitTransition(new Explode());
        setContentView(R.layout.item_show_case);
        mToolBar = (Toolbar) findViewById(R.id.my_toolbar);

        setSupportActionBar(mToolBar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Product Details");
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mTabLayout = (TabLayout) findViewById(R.id.tl_PD_TabLayout);
        mViewPager = (ViewPager) findViewById(R.id.vp_product_details);

        mPDViewPagerAdapter = new PDViewPagerAdapter(getSupportFragmentManager());
        mPDViewPagerAdapter.addFragment(new Description1(), "Description 1");
        mPDViewPagerAdapter.addFragment(new Description2(), "Description 2");
        mPDViewPagerAdapter.addFragment(new Description3(), "Description 3");

        mViewPager.setAdapter(mPDViewPagerAdapter);
        mViewPager.setCurrentItem(0);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}

package com.codes.andromed.mapplication.home.adapters.listviewadapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codes.andromed.mapplication.AnimatedExpandableListView;
import com.codes.andromed.mapplication.R;
import com.codes.andromed.mapplication.models.Product;
import com.codes.andromed.mapplication.productdetail.ProductDetails;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Utilisateur on 15/12/2016.
 */
public class AnimatedEXPLVAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

    private Context mContext;

    private ChildrenViewHolder childrenViewHolder;
    private CategoriesViewHolder categoriesViewHolder;

    private LayoutInflater childLayoutInflater = null;
    private LayoutInflater categoryLayoutInflater = null;

    private List<String> categoriesTitles;
    private HashMap<String, List<Product>> childrenTitles;

    static class ChildrenViewHolder {
        public TextView tvCenterTopTitle;
        public TextView tvCenterBottomTitle;
        public TextView tvLeftTitle;
        public TextView tvRightTitle;
        public ImageView ivLeftImage;
        public ImageView ivRightImage;
    }

    static class CategoriesViewHolder {
        public ImageView categoryIcon;
        public TextView categoryTitle;
    }

    public AnimatedEXPLVAdapter(Context context, List<String> categoriesTitles,
                                     HashMap<String, List<Product>> childrenTitles) {
        this.mContext = context;
        this.categoriesTitles = categoriesTitles;
        this.childrenTitles = childrenTitles;
        this.childLayoutInflater = LayoutInflater.from(context);
        this.categoryLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        //return items.get(groupPosition).items.get(childPosition);
        return childrenTitles.get(categoriesTitles.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            childrenViewHolder = new ChildrenViewHolder();
            convertView = childLayoutInflater.inflate(R.layout.lv_item_sfr, null);
            childrenViewHolder.tvCenterTopTitle = (TextView) convertView.findViewById(R.id.tv_center_top_title);
            childrenViewHolder.tvCenterBottomTitle = (TextView) convertView.findViewById(R.id.tv_center_bottom_title);
            childrenViewHolder.tvLeftTitle = (TextView) convertView.findViewById(R.id.tv_left_title);
            childrenViewHolder.tvRightTitle = (TextView) convertView.findViewById(R.id.tv_right_title);
            childrenViewHolder.ivLeftImage = (ImageView) convertView.findViewById(R.id.iv_left_image);
            childrenViewHolder.ivRightImage = (ImageView) convertView.findViewById(R.id.iv_right_image);
            convertView.setTag(childrenViewHolder);
        } else {
            childrenViewHolder = (ChildrenViewHolder) convertView.getTag();
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, ProductDetails.class),
                        ActivityOptions.makeSceneTransitionAnimation((Activity) mContext).toBundle());
            }
        });
        return convertView;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        //return items.get(groupPosition).items.size();
        return childrenTitles.get(categoriesTitles.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return categoriesTitles.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return categoriesTitles.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String categoryTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            categoriesViewHolder = new CategoriesViewHolder();
            convertView = categoryLayoutInflater.inflate(R.layout.category_header, null);
            categoriesViewHolder.categoryIcon = (ImageView) convertView.findViewById(R.id.iv_Category_Icon);
            categoriesViewHolder.categoryTitle = (TextView) convertView.findViewById(R.id.tv_Category_Title);
            convertView.setTag(categoriesViewHolder);
        } else {
            categoriesViewHolder = (CategoriesViewHolder) convertView.getTag();
        }

        if (categoryTitle != null) {
            categoriesViewHolder.categoryTitle.setText(categoryTitle);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }
}
